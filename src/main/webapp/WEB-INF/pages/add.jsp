<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adding articles</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="form-add">
            <h2>Добавление статьи</h2><br>
            <form:form method="POST" action="/add" modelAttribute="article">
                <form:hidden path="id" />
                <div class="form-group">
                    <label for="inputTitle">Заголовок</label>
                    <form:input path="title" cssClass="form-control" id="inputTitle" />
                </div>
                <div class="form-group">
                    <label for="inputDescription">Описание</label>
                    <form:textarea path="description" cssClass="form-control" id="inputDescription" />
                </div>
                <div class="form-group">
                    <label for="inputFile">Прикрепить изображение</label>
                </div>
                <div class="form-group">
                    <div class="wrap">
                        <label for="inputFile" class="camera"></label>
                    </div>
                    <input type="file" name="file" id="inputFile" class="hidden" />
                    <form:input path="image" cssClass="hidden" id="inputFile" />
                    <div class="buttons">
                        <a href="/" class="abort"></a>
                        <label for="button-submit" class="add"></label>
                        <input type="submit" id="button-submit" class="hidden" />
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
