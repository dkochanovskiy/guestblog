<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All articles</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans|Open+Sans+Condensed:300" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="header">
            <div class="header-left">
                <div class="logo"></div><br>
                <div class="article_text">Статьи, ${count}</div>
                <div class="article_date"></div>
            </div>
            <div class="header-right">
                <a href="add"><div class="add_article"></div></a>
            </div>
        </div>
        <div class="tape">
            <c:forEach var="article" items="${articles}">
                <div class="tape-inner">
                    <div class="tape-inner-content">
                        <div class="tape-inner-content-top" style="background: url('${pageContext.request.contextPath}/resources/img/content/${article.image}.jpg')"></div>
                        <div class="tape-inner-content-middle">
                            <div class="tape-inner-content-middle-title">${article.title}</div>
                            <div class="tape-inner-content-middle-date"><c:out value="${date}" /> <c:out value="${month}" />, <c:out value="${year}" /></div>
                        </div>
                        <hr>
                        <div class="tape-inner-content-bottom">
                            <div class="tape-inner-content-bottom-description">
                                    ${article.description}
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>