package ru.kochanovskiy.guestblog.services.interfaces;

import ru.kochanovskiy.guestblog.models.pojo.Article;

import java.util.List;

public interface ArticleService {

    void add(Article contact);

    void update(Article contact);

    Article get(Long id);

    List<Article> getAll();

    Long getCount();

    String getDate();

    String getMonth();

    String getYear();
}
