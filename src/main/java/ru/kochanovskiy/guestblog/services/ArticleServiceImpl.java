package ru.kochanovskiy.guestblog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kochanovskiy.guestblog.models.dao.ArticleDaoImpl;
import ru.kochanovskiy.guestblog.models.dao.SequenceDaoImpl;
import ru.kochanovskiy.guestblog.models.pojo.Article;
import ru.kochanovskiy.guestblog.services.interfaces.ArticleService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private final SequenceDaoImpl sequenceDaoImpl;
    private final ArticleDaoImpl articleDaoImpl;

    @Autowired
    public ArticleServiceImpl(SequenceDaoImpl sequenceDaoImpl, ArticleDaoImpl articleDaoImpl) {
        this.sequenceDaoImpl = sequenceDaoImpl;
        this.articleDaoImpl = articleDaoImpl;
    }

    public void add(Article contact) {
        contact.setId(sequenceDaoImpl.getNextSequenceId(Article.COLLECTION_NAME));
        articleDaoImpl.save(contact);
    }

    public void update(Article contact) {
        articleDaoImpl.save(contact);
    }

    public Article get(Long id) {
        return articleDaoImpl.get(id);
    }

    public List<Article> getAll() {
        return articleDaoImpl.getAll();
    }

    public Long getCount() {
        return articleDaoImpl.getCount();
    }

    public String getDate() {
        Date date = new Date(articleDaoImpl.getDateUNIX());
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        return sdf.format(date);
    }

    public String getMonth() {
        String month = "";
        Date date = new Date(articleDaoImpl.getDateUNIX());
        SimpleDateFormat sdf = new SimpleDateFormat("M");

        if(Integer.parseInt(sdf.format(date)) == 1){
            month = "янв";
        } else if (Integer.parseInt(sdf.format(date)) == 2){
            month = "фев";
        } else if (Integer.parseInt(sdf.format(date)) == 3){
            month = "мар";
        } else if (Integer.parseInt(sdf.format(date)) == 4){
            month = "апр";
        } else if (Integer.parseInt(sdf.format(date)) == 5){
            month = "май";
        } else if (Integer.parseInt(sdf.format(date)) == 6){
            month = "июнь";
        } else if (Integer.parseInt(sdf.format(date)) == 7){
            month = "июль";
        } else if (Integer.parseInt(sdf.format(date)) == 8){
            month = "авг";
        } else if (Integer.parseInt(sdf.format(date)) == 9){
            month = "сен";
        } else if (Integer.parseInt(sdf.format(date)) == 10){
            month = "окт";
        } else if (Integer.parseInt(sdf.format(date)) == 11){
            month = "ноя";
        } else if (Integer.parseInt(sdf.format(date)) == 12){
            month = "дек";
        }

        return month;
    }

    public String getYear() {
        Date date = new Date(articleDaoImpl.getDateUNIX());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(date);
    }
}