package ru.kochanovskiy.guestblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kochanovskiy.guestblog.services.ArticleServiceImpl;

@Controller
@RequestMapping("/")
public class AllArticleController {

    private final ArticleServiceImpl articleServiceImpl;

    @Autowired
    public AllArticleController(ArticleServiceImpl articleServiceImpl) {
        this.articleServiceImpl = articleServiceImpl;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showAll() {
        ModelAndView modelAndView = new ModelAndView("tape");

        modelAndView.addObject("articles", articleServiceImpl.getAll());
        modelAndView.addObject("year", articleServiceImpl.getYear());
        modelAndView.addObject("month", articleServiceImpl.getMonth());
        modelAndView.addObject("date", articleServiceImpl.getDate());
        modelAndView.addObject("count", articleServiceImpl.getCount());

        return modelAndView;
    }
}
