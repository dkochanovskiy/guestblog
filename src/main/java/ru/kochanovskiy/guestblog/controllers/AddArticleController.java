package ru.kochanovskiy.guestblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kochanovskiy.guestblog.models.pojo.Article;
import ru.kochanovskiy.guestblog.services.ArticleServiceImpl;

@Controller
@RequestMapping("/")
public class AddArticleController {

    private final ArticleServiceImpl articleServiceImpl;

    @Autowired
    public AddArticleController(ArticleServiceImpl articleServiceImpl) {
        this.articleServiceImpl = articleServiceImpl;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView showAddForm() {
        return new ModelAndView("add", "article", new Article());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addArticle(@ModelAttribute("article") Article article) {
        if(article.getId() == null) articleServiceImpl.add(article);
        else articleServiceImpl.update(article);

        return "redirect:/successful";
    }
}
