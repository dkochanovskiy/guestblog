package ru.kochanovskiy.guestblog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class SuccessfulArticleController {

    @RequestMapping("/successful")
    public String successfulArticle() {
        return "successful";
    }
}
