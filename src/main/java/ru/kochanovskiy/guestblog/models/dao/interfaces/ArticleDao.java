package ru.kochanovskiy.guestblog.models.dao.interfaces;

import ru.kochanovskiy.guestblog.models.pojo.Article;
import java.util.List;

public interface ArticleDao {

    void save(Article article);

    Article get(Long id);

    List<Article> getAll();

    Long getCount();

    Long getDateUNIX();
}
