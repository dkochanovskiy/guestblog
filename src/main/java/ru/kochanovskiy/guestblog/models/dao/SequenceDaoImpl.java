package ru.kochanovskiy.guestblog.models.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import ru.kochanovskiy.guestblog.models.dao.interfaces.SequenceDao;
import ru.kochanovskiy.guestblog.models.pojo.Sequence;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
public class SequenceDaoImpl implements SequenceDao {

    private final MongoOperations mongoOperations;

    @Autowired
    public SequenceDaoImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public Long getNextSequenceId(String key) {
        org.springframework.data.mongodb.core.query.Query query = new org.springframework.data.mongodb.core.query.Query(where("id").is(key));

        Update update = new Update();
        update.inc("sequence", 1);

        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);

        Sequence sequence = mongoOperations.findAndModify(query, update, options, Sequence.class);

        return sequence.getSequence();
    }

}
