package ru.kochanovskiy.guestblog.models.dao;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import ru.kochanovskiy.guestblog.models.dao.interfaces.ArticleDao;
import ru.kochanovskiy.guestblog.models.pojo.Article;

import java.util.List;

@Repository
public class ArticleDaoImpl implements ArticleDao {

    private final MongoOperations mongoOperations;

    @Autowired
    public ArticleDaoImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public void save(Article article) {
        mongoOperations.save(article);
    }

    public Article get(Long id) {
        return mongoOperations.findOne(Query.query(Criteria.where("id").is(id)), Article.class);
    }

    public List<Article> getAll() {
        return mongoOperations.findAll(Article.class);
    }

    public Long getCount(){
        Query query = new Query();
        return mongoOperations.count(query, Article.class);
    }

    public Long getDateUNIX(){
        return ObjectId.get().getTime();
    }
}