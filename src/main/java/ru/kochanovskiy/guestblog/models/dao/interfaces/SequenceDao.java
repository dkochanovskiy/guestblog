package ru.kochanovskiy.guestblog.models.dao.interfaces;

public interface SequenceDao {

    Long getNextSequenceId(String key);
}
